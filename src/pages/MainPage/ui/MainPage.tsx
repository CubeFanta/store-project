import classNames from 'classnames';
import Slider from 'shared/ui/Slider/ui/Slider';
import { Text } from 'shared/ui/Text/Text';
import { Button } from 'shared/ui/Button/Button';
import { Modal } from 'shared/ui/Modal/Modal';
import { useState } from 'react';
import cls from './MainPage.module.scss';

const slides = [
	{ img: 'https://hasky.online/wp-content/uploads/2023/10/7405-350x350.jpg' },
	{ img: 'https://hasky.online/wp-content/uploads/2023/09/8957-350x350.jpg' },
	{ img: 'https://hasky.online/wp-content/uploads/2023/09/7221-1-350x350.jpg' },
];

const MainPage = () => {
	const nan = NaN;

	return (
		<div className={classNames(cls.Main)}>
			<Slider slides={slides} />
		</div>
	);
};

export default MainPage;