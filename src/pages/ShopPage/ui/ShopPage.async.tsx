import { lazy } from 'react';

export const ShopPageAsync = lazy(() => import('./ShopPage'));

// export const ShopPageAsync = lazy(() => new Promise((resolve) => {
// 	// @ts-ignore
// 	setTimeout(() => resolve(import('./ShopPage')), 1500);
// }));