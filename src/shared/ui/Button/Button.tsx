import classNames from 'classnames';
import { ButtonHTMLAttributes, ReactNode, memo } from 'react';
import cls from './Button.module.scss';

export enum ButtonTheme {
	VIEW_ONE = 'view_one',
	VIEW_TWO = 'view_two',
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
	theme?: ButtonTheme;
	children?: ReactNode;
}

export const Button = memo((props: ButtonProps) => {
	const {
		theme,
		children,
		...otherProps
	} = props;

	return (
		<button
			type="button"
			className={classNames(cls.Button, {}, cls[theme])}
			{...otherProps}
		>
			{children}
		</button>
	);
});