import classNames from 'classnames';
import { ReactNode, memo } from 'react';
import cls from './Text.module.scss';

export enum TextSize {
	S = 'size_s',
	M = 'size_m',
	L = 'size_l',
}

export enum TextAlign {
	RIGHT = 'right',
	LEFT = 'left',
	CENTER = 'center',
}

interface TextProps {
	className?: string;
	children?: ReactNode;
	aling?: TextAlign;
	size?: TextSize;
}

export const Text = memo((props: TextProps) => {
	const {
		className,
		children,
		aling,
		size,
	} = props;

	const mods: Record<string, boolean | string | undefined> = {
		[cls[aling]]: true,
		[cls[size]]: true,
	};

	return (
		<p className={classNames(cls.Text, mods, [className])}>
			{children}
		</p>
	);
});