import { ReactNode, memo, useState } from 'react';
import { Button } from 'shared/ui/Button/Button';

import cls from './Slider.module.scss';

interface Slides {
	img: string;
}

interface SliderProps {
	slides?: Slides[];
	children?: ReactNode;
}

const PAGE_WIDTH = 350;

const Slider = memo((props: SliderProps) => {
	const [offset, setOffset] = useState<number>(0);

	const {
		slides,
		children,
	} = props;

	const clickLeft = () => {
		setOffset((currentOffset) => {
			const newOffset = currentOffset + PAGE_WIDTH;

			return Math.min(newOffset, 0);
		});
	};

	const clickRight = () => {
		setOffset((currentOffset) => {
			const newOffset = currentOffset - PAGE_WIDTH;

			const maxOffset = -(PAGE_WIDTH * (slides.length - 1));

			return Math.max(newOffset, maxOffset);
		});
	};

	return (
		<div className={cls.container}>
			<div className={cls.window}>
				<Button
					onClick={clickLeft}
				>
					left
				</Button>
				<Button
					onClick={clickRight}
				>
					right
				</Button>
				<div className={cls.items} style={{ transform: `translateX(${offset}px)` }}>
					{slides.map((item, index) => (
						<img
							alt="img"
							src={item.img}
							key={index}
							className={cls.img}
						/>
					))}
				</div>
			</div>

		</div>
	);
});

export default Slider;