import { configureStore } from '@reduxjs/toolkit';
import { StateSchema } from './StateShema';

export const store = configureStore<StateSchema>({
	reducer: {},

});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch