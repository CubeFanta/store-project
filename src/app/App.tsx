import classNames from 'classnames';
import { Header } from 'widgets/Header';
import { Footer } from 'widgets/Footer';
import { Suspense } from 'react';
import { AppRouter } from './providers/router';
import './styles/index.scss';

const App = () => (
	<div className={classNames('app')}>
		<Suspense fallback="">
			<Header />
			<div className="content-page">
				<AppRouter />
			</div>
			<Footer />
		</Suspense>
	</div>

);

export default App;