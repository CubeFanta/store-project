import CallLogotype from 'shared/assets/icons/call.svg';
import classNames from 'classnames';
import { Modal } from 'shared/ui/Modal/Modal';
import { useState } from 'react';
import { Text, TextAlign, TextSize } from 'shared/ui/Text/Text';
import { Button, ButtonTheme } from 'shared/ui/Button/Button';
import { Input } from 'shared/ui/Input/Input';
import cls from './CallLogo.module.scss';

interface CallLogoProps {
	className?: string;
}

export const CallLogo = (props: CallLogoProps) => {
	const { className } = props;
	const [isOpen, setIsOpen] = useState(false);
	const [value, setValue] = useState('');

	const onChange = (val: string) => {
		setValue(val);
	};

	return (
		<div className={classNames(cls.CallLogo, {}, [className])}>
			<CallLogotype
				className={classNames(cls.svg)}
				onClick={() => setIsOpen(true)}
			/>
			<Modal isOpen={isOpen} onClose={() => setIsOpen(false)} lazy>
				<div className={cls.CallForm}>
					<Text size={TextSize.L} aling={TextAlign.CENTER}>Заказать обратный звонок</Text>
					<Input placeholder="Имя" className={cls.input} />
					<Input placeholder="E-mail" className={cls.input} />
					<Input placeholder="Номер" className={cls.input} />
					<Button theme={ButtonTheme.VIEW_TWO}>Заказать Звонок</Button>
				</div>
			</Modal>
		</div>
	);
};