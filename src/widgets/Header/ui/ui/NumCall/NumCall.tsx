import { Text, TextSize } from 'shared/ui/Text/Text';

export const NumCall = () => (
	<Text
		size={TextSize.S}
	>
		+7(965) 555-55-55
	</Text>
);