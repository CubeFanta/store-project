import classNames from 'classnames';
import { AppLink } from 'shared/ui/Link/AppLink';
import BasketLogotype from 'shared/assets/icons/basket.svg';
import cls from './BasketLogo.module.scss';

interface BasketLogoProps {
	className?: string;
}

export const BasketLogo = (props: BasketLogoProps) => {
	const { className } = props;

	return (
		<div className={classNames(cls.basket, {}, [className])}>
			<AppLink to="/">
				<BasketLogotype />
			</AppLink>
		</div>
	);
};