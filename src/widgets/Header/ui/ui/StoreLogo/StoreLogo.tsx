import LogoItem from 'shared/assets/icons/Logo.svg';
import { AppLink } from 'shared/ui/Link/AppLink';

export const StoreLogo = () => (
	<AppLink to="/">
		<LogoItem />
	</AppLink>
);