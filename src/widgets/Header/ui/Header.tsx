import classNames from 'classnames';
import { memo } from 'react';
import { Navbar } from 'widgets/Navbar';
import cls from './Header.module.scss';
import { StoreLogo } from './ui/StoreLogo/StoreLogo';
import { BasketLogo } from './ui/BascetLogo';
import { CallLogo } from './ui/CallLogo';
import { NumCall } from './ui/NumCall';

export const Header = memo(() => (
	<header className={classNames(cls.header)}>
		<div className={classNames(cls.container)}>
			<div className={classNames(cls.row)}>
				<StoreLogo />
				<Navbar />
				<CallLogo />
				<NumCall />
				<BasketLogo />
			</div>
		</div>
	</header>
));