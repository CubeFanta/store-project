import classNames from 'classnames';
import { memo } from 'react';
import { AppLink } from 'shared/ui/Link/AppLink';
import cls from './Navbar.module.scss';

export const items = [
	{ text: 'Главная', href: '/' },
	{ text: 'Магазин', href: '/about' },
	{ text: 'О бренде', href: '/brend' },
	{ text: 'Контакты', href: '/contacts' },
];

export const Navbar = memo(() => (
	<nav className={cls.navbar}>
		<ul className={classNames(cls.list)}>
			{
				items.map((item) => (
					<li key={item.text} className={cls.item}>
						<AppLink className={classNames(cls.link)} to={item.href}>
							{item.text}
						</AppLink>
					</li>
				))
			}
		</ul>
	</nav>
));