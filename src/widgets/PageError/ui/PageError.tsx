import classNames from 'classnames';
import { Button, ButtonTheme } from 'shared/ui/Button/Button';
import cls from './PageError.module.scss';

interface PageErrorProps {
	className?: string;
}

export const PageError = ({ className }: PageErrorProps) => {
	const reloadPage = () => {
		// eslint-disable-next-line
		location.reload();
	};

	return (
		<div className={classNames(cls.PageError, {}, [className])}>
			<p>Произошла непредвиденная ошибка</p>
			<Button
				onClick={reloadPage}
				theme={ButtonTheme.VIEW_TWO}
			>
				Обновить страницу
			</Button>
		</div>
	);
};