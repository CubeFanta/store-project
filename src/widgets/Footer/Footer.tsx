import classNames from 'classnames';
import { Navbar } from 'widgets/Navbar';
import Logotype from 'shared/assets/icons/Logo.svg';
import { AppLink } from 'shared/ui/Link/AppLink';
import { Text } from 'shared/ui/Text/Text';
import IconInst from 'shared/assets/icons/instagram (1) 1.svg';
import IconFace from 'shared/assets/icons/facebook (1) 1.svg';
import IconTwit from 'shared/assets/icons/twitter (1) 1.svg';
import IconCard from 'shared/assets/icons/visa-mastercard 1.svg';
import cls from './Footer.module.scss';

interface FooterProps {
	className?: string;
}

export const infoItems = [
	{ text: '© Все права защищены' },
	{ text: 'Политика конфиденциальности' },
	{ text: 'Публичная оферта' },
];

export const dressItems = [
	{ text: 'Пальто' },
	{ text: 'Свитшоты' },
	{ text: 'Кардиганы' },
	{ text: 'Толстовки' },
];

export const Footer = (props: FooterProps) => {
	const { className } = props;

	return (
		<div className={classNames(cls.Footer, {}, [className])}>
			<div className={classNames(cls.container)}>
				<div className={classNames(cls.row)}>
					<Logotype />
					<Navbar />
					<div>
						<Text>+7 (964) 555-55-55</Text>
						<Text>hello@womazing.com</Text>
					</div>
				</div>
				<div className={classNames(cls.row)}>
					<nav>
						<ul>
							{infoItems.map((item) => (
								<li key={item.text}>
									<AppLink to="/">
										{item.text}
									</AppLink>
								</li>
							))}
						</ul>
					</nav>
					<nav>
						<ul>
							{dressItems.map((item) => (
								<li key={item.text}>
									<AppLink to="/">
										{item.text}
									</AppLink>
								</li>
							))}
						</ul>
					</nav>
					<div>
						<div className={classNames(cls.social_icon)}>
							<IconInst />
							<IconFace />
							<IconTwit />
						</div>
						<IconCard />
					</div>
				</div>
			</div>
		</div>
	);
};